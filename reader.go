package stringly

import(
	"fmt"
	"os"
	"strings"
)
//Take 'input' location and get text from those files. Populate the package-wide string
func LoadTDat(target string) BigString {
	//TODO: check for terminating slash in file location and handle	appropriately
	
	var tDat string = ""

	outputDirRead, _ := os.Open(target)
	files, _ := outputDirRead.ReadDir(0)
	
	for _,file := range files{
		if !strings.Contains(file.Name(), ".txt"){
			continue
		}
	
		txt, txtErr := os.ReadFile(target+file.Name())
		if txtErr != nil{
			fmt.Println("Error:", txtErr)
		}	
	
		tDat = tDat + " " + string(txt)
	}

	return BigString(tDat)
}
