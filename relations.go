package stringly

//int is distance from master, float64 is score
type Relation map[string][]int

//For containing the relations to other words that this word has
type RelationCont struct{
	MasterWord string //May not be necessary. Leaving for now.
	Relations map[string]Relation //string is word related to, Relations are relation scores
}
