package stringly

import(
	"strings"
	"regexp"
	"fmt"
	"sync"
)

func SplitSentences(input string) []string {
	// Define the delimiters as ".", "?", and "!"
	delimiters := ".?!"

	// Split the input string using the delimiters
	substrings := strings.FieldsFunc(input, func(r rune) bool {
		return strings.ContainsRune(delimiters, r)
	})

	return substrings
}

func GetWords(input string) []string {
	retmap := make(map[string]bool)
	retSlc := []string{}

	input = strings.ToLower(input)

	regex := regexp.MustCompile("[^a-zA-Z0-9\\s]+")
	input = regex.ReplaceAllString(input, "")

	flds := strings.Fields(input)

	//Use map to do the filter work. Bools are not used
	for _,wrd := range flds{
		retmap[wrd]=true
	}

	for key,_ := range retmap{
		retSlc = append(retSlc, key)
	}

	return retSlc
}

//Returns a map of string keys and ints that represent the prevalence of that string
func GetWordFreq(input string) map[string]int{
	retmap := make(map[string]int)

	input = strings.ToLower(input)
	regex := regexp.MustCompile("[^a-zA-Z0-9\\s]+")
	input = regex.ReplaceAllString(input, "")
	flds := strings.Fields(input)

	//Use map to do the filter work. Bools are not used
	for _,wrd := range flds{
		if _,ok := retmap[wrd]; !ok {
			retmap[wrd] = 1
		}else{
			retmap[wrd] = retmap[wrd] + 1
		}
	}

	return retmap
}

func GetChars(input string) map[string]int{
	retmap := make(map[string]int)
	chars := strings.Split(input,"")
	for _,char := range chars{
		if _,ok := retmap[char]; !ok{
			retmap[char] = 1
		}else{
			retmap[char] = retmap[char]+1
		}
	}
	return retmap
}

//Return a map of English letters and numbers and their prevalence 
func GetAlphaneumeric(input string) map[string]int{
	retmap := make(map[string]int)

	regex := regexp.MustCompile("[^a-zA-Z0-9]+")
	input = regex.ReplaceAllString(input, "")

	chars := strings.Split(input,"")
	for _,char := range chars{
		if _,ok := retmap[char]; !ok{
			retmap[char] = 1
		}else{
			retmap[char] = retmap[char]+1
		}
	}

	return retmap
}

//Returns a map of relations between 'sub' and all words in 'big'
func distanceBetweenWords(big, sub string) (map[string][]int, error) {

	var mx sync.Mutex
	var wg sync.WaitGroup
	retMap := make(map[string][]int)
	regex  := regexp.MustCompile("[^a-zA-Z0-9\\s]+")
	
	//Process big to be clean and lower-case
	reped := regex.ReplaceAllString(big, "")
	reped  = strings.ToLower(reped)
	words := strings.Fields(reped)

	//Make sure sub is clean
	sub = regex.ReplaceAllString(sub, "")
	sub = strings.ToLower(sub)

	//Get the indexes of 'sub' in 'big'
	subNdx := []int{}
	//fmt.Println("Big:", words, "\n\n")
	//fmt.Println("Sub:", sub)
	for ndx,word := range words{
		if word == sub{
			subNdx = append(subNdx, ndx)
		}
	}

	if len(subNdx) <1 {
		errTxt := fmt.Sprintf("dbw fail. No instances of \n\"%s\"\n found in \n\"%s\"\n", sub, big)
		return retMap, fmt.Errorf(errTxt)
	}

	//fmt.Println("Indexes:", subNdx)

	wg.Add(len(subNdx)*2)
	for _,startNdx := range subNdx{
			go func(){
				defer wg.Done()
				for i := startNdx; i < len(words); i++{
					if words[i] == sub{
						continue
					}

					mx.Lock()
					if _,ok := retMap[words[i]]; !ok{
						//mx.Lock()
							retMap[words[i]] = []int{i}
						//mx.Unlock()
					}else{
						tmp := retMap[words[i]]
						tmp = append(tmp, i)
						//mx.Lock()
							retMap[words[i]] = tmp
						//mx.Unlock()
					}
					mx.Unlock()
				}
				//return
			}()

			//Get pre-indexes
			go func(){
				//wg.Add(1)
				defer wg.Done()
				for i := startNdx; i > -1; i=i-1{
									
					if words[i] == sub{
						continue
					}

					mx.Lock()
					if _,ok := retMap[words[i]]; !ok{
						
							retMap[words[i]] = []int{0-i}
						
					}else{
						tmp := retMap[words[i]]
						tmp = append(tmp, 0-i)
						
							retMap[words[i]] = tmp
						
					}
					mx.Unlock()
				}
				//return
			}()
			//return
		//}()
	}

	wg.Wait()
	return retMap,nil
}  
  