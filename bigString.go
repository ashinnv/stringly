package stringly

import(
	"regexp"
)

type BigString string //Just so we can use methods here

func (str BigString)GetWords()[]string{
	return GetWords(string(str))
}

//Produces a map with each word in str as key with the number of occurrences as the value
func (str BigString)GetWordFreq() map[string]int {
	return GetWordFreq(string(str))
}

func (str *BigString)GetSentences()[]string{
	regex := regexp.MustCompile(`[.?!]`)
	return(regex.Split(string(*str),-1))
}

//Returns a map of single-character strings that represent the characters in the data and the prevalence of them
func (str BigString)GetChars()map[string]int{
	return GetChars(string(str))
}

func (str BigString)GetAlphaneumeric() map[string]int{
	return GetAlphaneumeric(string(str))
}

func (str BigString)GetWordRelations() map[string]RelationCont{
	words := str.GetWords()
	rets  := []RelationCont
	for _,word := range words{
		
		rels := distanceBetweenWords(str, word)

		tCon := RelationCont{word,}
	}
}

/*

//For containing the relations to other words that this word has
type RelationCont struct{
	MasterWord string //May not be necessary. Leaving for now.
	Relations map[string]Relation //string is word related to, Relations are relation scores
}

*/